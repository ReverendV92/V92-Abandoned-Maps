
FS_Moon

A map that I started at the request of a friend who wanted to make a Tiramisu RP gamemode based on the indie movie 'Moon'

The movie has Kevin Spacey as a robot in it or something. I've heard it's good.

Takes place at a fictional 'Sarang' moon base where all is not as it seems.

The RP mode was intended to be taking place after the conclusion of the movie.
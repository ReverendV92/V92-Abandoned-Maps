# V92-Abandoned-Maps
Maps from years past that I am releasing to the public

All of these maps have been sorted into per-project folders.

I wrote a short text file in each one with a bit of backstory or what the project is and why it was shitcanned.

If you use any part of it, throw me a mention in the description and send me a link to it so I can take a look at it.

# License/Agreement/Legaleeze

By downloading this archive, in part or in whole, you agree to the following:

If you use any part of these map files contained here within, either in part, in full, or in idea, that you will acknowledge my contribution or influence in your project's description or in the map itself.

I will be notified of any released content using the content within so that I can see what you did with it.

You will not re-upload, re-host, re-release, or re-distribute this archive, in part or in whole, to any other site on the internet, with or without my permission.

I retain the right to change this license as I see fit and makesaid changes retroactively active.
